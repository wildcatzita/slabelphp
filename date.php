<?php
/**
 * Created by PhpStorm.
 * User: Zita
 * Date: 06.09.2015
 * Time: 15:24
 */
$str = [
    "time" => date("H:i:s"),
    "data" => date("j.m.Y"),
    "dayOfWeek" => date("N")
];
echo json_encode($str);