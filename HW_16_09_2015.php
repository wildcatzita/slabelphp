<?php
/**
 * Created by PhpStorm.
 * User: Zita
 * Date: 20.09.2015
 * Time: 11:23
 */

class Point {
    protected $x1, $y1;

    public function __construct($x1, $y1) {
        $this->x1 = intval($x1);
        $this->y1 = intval($y1);
    }

    public function setX1($x1) {
        $this->x1 = intval($x1);
    }
    public function setY1($y1) {
        $this->y1 = intval($y1);
    }

    public function getX1() {
        return  $this->x1;
    }
    public function getY1() {
        return  $this->y1;
    }

    public function printPoints() {
        echo "x1 = [" . $this->getX1() . "], y1 = [" . $this->getY1() . "]<br/>";
    }
}

class Line extends Point {
    protected $x2, $y2;

    public function __construct($x1, $y1, $x2, $y2) {
        parent::__construct($x1, $y1);
        $this->x2 = intval($x2);
        $this->y2 = intval($y2);
    }

    public function setX2($x2) {
        $this->x2 = intval($x2);
    }
    public function setY2($y2) {
        $this->y2 = intval($y2);
    }

    public function getX2() {
        return  $this->x2;
    }
    public function getY2() {
        return  $this->y2;
    }

    public function printPoints() {
        echo "x1 = [" . $this->getX1() . "], y1 = [" . $this->getY1() . "], x2 = [" . $this->getX2() . "], y2 = [" . $this->getY1() . "]<br/>";
    }

    public function isOnLine(Point $point) {
        if ((($point->getX1() - $this->getX1())*($this->getY2() - $this->getY1())-($point->getY1() - $this->getY1())*($this->getX2() - $this->getX1()) == 0) && ($this->getX1() < $point->getX1() && $point->getX1() < $this->getX2())) {
            echo "Point[" .$point->getX1() . ":" . $point->getY1() . "] is on the Line([" . $this->getX1() . ":". $this->getY1() . "][" . $this->getX2() . ":". $this->getY2() . "])";
        } else {
            echo "Point[" .$point->getX1() . ":" . $point->getY1() . "] is not on the Line([" . $this->getX1() . ":". $this->getY1() . "][" . $this->getX2() . ":". $this->getY2() . "])";
        }
    }
}

class Triangle extends Line {
    protected $x3, $y3;
    public function __construct($x1, $y1, $x2, $y2, $x3, $y3) {
        parent::__construct($x1, $y1, $x2, $y2);
        $this->x3 = intval($x3);
        $this->y3 = intval($y3);
    }

    public function setX3($x3) {
        $this->x3 = intval($x3);
    }
    public function setY3($y3) {
        $this->y3 = intval($y3);
    }

    public function getX3() {
        return  $this->x3;
    }
    public function getY3() {
        return  $this->y3;
    }

    public function printPoints() {
        echo "x1 = [" . $this->getX1() . "], y1 = [" . $this->getY1() . "], x2 = [" . $this->getX2() . "], y2 = [" . $this->getY2() . "], x3 = [" . $this->getX3() . "], y3 = [" . $this->getY3() . "]<br/>";
    }
}

class Rectangle extends Triangle {
    protected $x4, $y4;
    public function __construct($x1, $y1, $x2, $y2, $x3, $y3, $x4, $y4) {
        parent::__construct($x1, $y1, $x2, $y2, $x3, $y3);
        $this->x4 = intval($x4);
        $this->y4 = intval($y4);
    }

    public function setX4($x4) {
        $this->x4 = intval($x4);
    }
    public function setY4($y4) {
        $this->y4 = intval($y4);
    }

    public function getX4() {
        return  $this->x4;
    }
    public function getY4() {
        return  $this->y4;
    }

    public function printPoints() {
        echo "x1 = [" . $this->getX1() . "], y1 = [" . $this->getY1() . "], x2 = [" . $this->getX2() . "], y2 = [" . $this->getY2() . "], x3 = [" . $this->getX3() . "], y3 = [" . $this->getY3() . "], x4 = [" . $this->getX4() . "], y4 = [" . $this->getY4() . "]<br/>";
    }
}

$point1 = new Point();
$line1 = new Line(2, 1, 7, 6);
$triangle1 = new Triangle(0, 0, 10, 0, 5, 10);
$rectangle1 = new Rectangle(0, 0, 10, 0, 10, 10, 0, 10);


$point1->setX1(5);
$point1->setY1(4);
$point1->printPoints();
$line1->printPoints();
$triangle1->printPoints();
$rectangle1->printPoints();

$line1->isOnLine($point1);
