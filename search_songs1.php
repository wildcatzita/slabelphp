<?php
/**
 * Created by PhpStorm.
 * User: Zita
 * Date: 06.09.2015
 * Time: 20:49
 */
session_start();
//var_dump($_GET['search_songs']);
//var_dump($_SESSION['token']);
if (isset($_SESSION['token']) and isset($_GET['search_songs'])) {
    $songs_count = 20;
    $ch = curl_init();
    curl_setopt_array($ch, [
        CURLOPT_URL => 'http://api.pleer.com/index.php',
        CURLOPT_HTTPHEADER => ['Authorization: Bearer ' . $_SESSION['token']] ,
        CURLOPT_POST => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POSTFIELDS => [
            'method' => 'tracks_search',
            'query' => $_GET['search_songs'],
            'page' => 1,
            'result_on_page' => $songs_count,
            'quality' => 'best'
        ],
    ]);
    $tracks = curl_exec($ch);
    $tracks = json_decode($tracks, true);
    //var_dump($tracks);

        foreach ($tracks['tracks'] as $track) {
            if (is_array($track)) {
                echo "<li><a href='song.php?track_id={$track['id']}' style='text-decoration: none; color: black'><span style='font-weight: bold'>{$track['artist']}</span><span style='color: grey; padding-left: 20px'>{$track['track']}</span></a></li>";
            }
        }
}
