<?php
/**
 * Created by PhpStorm.
 * User: Zita
 * Date: 26.09.2015
 * Time: 12:10
 */

//2. Создать несколько дат и интервалов, вывести на экран в разном формате. Отнять от даты интервал, добавить к дате интервал.
/*
$dt = new DateTime('1985-08-28 00:00:00');
$now = new DateTime();
$int = new DateInterval('PT12H');
$int1 = new DateInterval('P30Y');

echo $dt->format(DATE_COOKIE) . "<br>";
echo $now->format(DATE_RFC822) . "<br>";
echo $now->sub($int)->format(DATE_RFC822) . "<br>";
echo $dt->add($int)->format(DATE_RFC822) . "<br>";
echo $dt->add($int1)->format(DATE_RSS) . "<br>";
*/


// 3. Сделать форму, пользователь вводит свою точную дату рождения, можно сделать либо input type=”text” либо 3 select в котором будет день месяц и год, если ему больше 21 лет то вывести сообщение,
// “вы можете приобрести алкоголь”, в обратном случае вывести что то другое :) Использовать класс DateTime

?>
<form method="post">
    <label>Введите дату рождения </label><input type="text" name="data">
    <input type="submit" value="Проверить возраст">
</form>

<?php
    if (isset($_POST['data'])) {
        $now = new DateTime();
        //echo $_POST['data'];
        $dt = new DateTime($_POST['data']);
        //echo $dt->format('d-m-Y');
        if ($now->diff($dt)->y >= 21) {
            ?>
                <div>
                    <?php echo "<p>Вы можете приобрести алкоголь)))</p>";?>
                </div>
            <?php
        } else {
            ?>
            <div>
                <?php echo "<p>Вы не можете приобрести алкоголь(((</p>";?>
            </div>
            <?php
        }
    }
?>

