<?php header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('UTF-8');
/**
 * Created by PhpStorm.
 * User: Zita
 * Date: 05.09.2015
 * Time: 22:21
 */

?>
<!DOCTYPE HTML>
<head>
    <meta charset="utf-8">
</head>
    <form method="post" action="">
        <?php $select = ['Kiev'=>'Kiev', 'Kharkiv'=>'Kharkiv', 'Lviv'=>'Lviv'] ?>
        <select name="town">
            <?php foreach($select as $key=>$value) {
                echo "<option " . " value='$key'>$value</option>";
            } ?>
        </select>
        <select name="days">
            <?php for ($i = 1; $i < 17; $i++) {
                echo "<option " . " value='$i'>$i</option>";
            } ?>
        </select>
        <input type="submit" name="submit" />
    </form>
<?php
if (isset($_POST['submit']) && isset($_POST['town'])) {
    $ch = curl_init();
    $days = isset($_POST['days']) ? $_POST['days'] : 1;
    $url = "http://api.openweathermap.org/data/2.5/weather?q=" . $_POST['town'] . "&mode=json&units=metric&cnt=" . $days;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');

    $data = curl_exec($ch);
    $data = json_decode($data, true);

    echo $data['weather']['0']['main'] . " weather in " . $_POST['town'] . ". Temperature is " . $data['main']['temp'] . " for " . $days . " day(s)";
}
?>