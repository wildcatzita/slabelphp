<?php
class MyClass {
    private $data = [];
    private $strout;

    public function __set($name, $value) {
        if (is_int($value)) {
            if ($value % 2) {
                $this->data[$name] = 1;
            } else {
                $this->data[$name] = 0;
            }
        }
    }

    public function __get($name) {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
    }

    public function __isset($name) {
        return isset($this->data[$name]);
    }

    public function __unset($name) {
        if (isset($this->data[$name])) {
            unset($this->data[$name]);
        }
    }

    public function __toString() {
        foreach($this->data as $key => $val) {
            $this->strout .= $key . ':' . $val . ' ';
        }
        return $this->strout;
    }

    public function __invoke($value) {
        $i = 0;
        $tmp = '';
        if ($value == 0) {
            foreach($this->data as $key => $item) {
                if ($i % 2 == 0) {
                    $this->strout .= $key . ' ';
                }
                $i++;
            }
            return $this->strout;
        } else if ($value) {
            foreach($this->data as $key => $item) {
                if ($i % 2) {
                    $this->strout .= $key . ' ';
                }
                $i++;
            }
            return $this->strout;
        } else {
            return false;
        }
    }
}

$a = New MyClass();
$a->foo = 2;
$a->foo1 = 3;
$a->foo2 = 3;
$a->foo3 = 3;
$a->foo33 = 4;
//echo $a->foo1;
echo $a;
echo $a(0);
unset($a->foo33);
