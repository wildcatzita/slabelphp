<?php
/**
 * Created by PhpStorm.
 * User: Zita
 * Date: 06.09.2015
 * Time: 17:23
 */

session_start();
$url = "http://api.pleer.com/token.php";

$ch = curl_init($url);
curl_setopt_array($ch, [
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POST => true,
    CURLOPT_USERPWD => '442695:IwwvypQf2QypB29gsHf8',
    CURLOPT_POSTFIELDS => [
        "grant_type" => "client_credentials"
    ],
]);

if($data = curl_exec($ch)) {
    $data = json_decode($data, true);
    $token = $data['access_token'];
    $_SESSION['token'] = $token;
    //var_dump($token);

    curl_setopt_array($ch, [
        CURLOPT_URL => 'http://api.pleer.com/index.php',
        CURLOPT_HTTPHEADER => ['Authorization: Bearer ' . $token] ,
        CURLOPT_POST => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POSTFIELDS => [
            'method' => 'get_top_list',
            'language' => 'en',
            'page' => 1,
            'list_type' => 1
        ],
    ]);

    if($tracks = curl_exec($ch)) {
        $tracks = json_decode($tracks, true);
        ?>
        <html>
        <head>
            <style>

                body {
                    background:white;
                }

                #search input[type="submit"]{
                    font: bold 12px Arial,Helvetica,Sans-serif;
                    color: #444;
                }

                #search input[type="text"] {
                    background: url("./img/search-dark.png") no-repeat 10px 6px #444;
                    border: 0 none;
                    font: bold 12px Arial,Helvetica,Sans-serif;
                    color: #777;
                    width: 150px;
                    margin-left: 25px;
                    padding: 6px 15px 6px 35px;
                    -webkit-border-radius: 20px;
                    -moz-border-radius: 20px;
                    border-radius: 20px;
                    text-shadow: 0 2px 2px rgba(0, 0, 0, 0.3);
                    -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
                    -moz-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
                    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
                    -webkit-transition: all 0.7s ease 0s;
                    -moz-transition: all 0.7s ease 0s;
                    -o-transition: all 0.7s ease 0s;
                    transition: all 0.7s ease 0s;
                }

                #search input[type="text"]:focus {
                    width: 200px;
                }

            </style>
            <script>
                function showHint(str) {
                    if (str.length == 0) {
                        document.getElementById("songs").innerHTML = "";
                        return;
                    } else {
                        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                document.getElementById("songs").innerHTML = xmlhttp.responseText;
                            }
                        }
                        xmlhttp.open("GET", "search_songs1.php?search_songs=" + str, true);
                        xmlhttp.send();
                    }
                }
            </script>
        </head>

        <body>
        <form method="get" action="search_songs.php" id="search">
            <input name="search_songs" type="text" size="40" placeholder="Search..." onkeyup="showHint(this.value)" />
            <input type="submit" value="Search">
        </form>
        <ol id="songs">
            <?php
            foreach ($tracks['tracks']['data'] as $track) {
                //var_dump($track);
                ?>
                <li>
                    <a href="song.php?track_id=<?php echo $track['id']?>" style="text-decoration: none; color: black"
                       onMouseOver="this.style.textDecoration='underline'"
                       onMouseOut="this.style.textDecoration='none'"
                        ><span style="text-align: left; font-weight: bold"><?php echo $track['artist'] . ' '?></span>
                        <span style="color: grey; padding-left: 15px"><?php echo $track['track']?></span>
                        <span style="padding-right: 20px; padding-left: 40px"><?php echo date("i:s", mktime(0, 0, $track['lenght']))?></span>
                        <span style="color: #444444"><?php echo $track['bitrate'] == "VBR" ? $track['bitrate'] : $track['bitrate'] . " Kb/s" ?></span>
                    </a></li>
                <?php
                //var_dump($item);
            }
            ?></ol>
        </body>
        </html>

        <?php
    }


        /*
    $arr = [];
    if($tracks = curl_exec($ch)) {
        $tracks = json_decode($tracks, true);
        foreach ($tracks['tracks'] as $track) {
            if (is_array($track)) {
                ?> <ol> <?php
                foreach ($track as $item) {
                    ?>
                    <li><a href="song.php?track_id=<?php echo $item['id']?>" style="text-decoration: none; color: black"
                           onMouseOver="this.style.textDecoration='underline'"
                           onMouseOut="this.style.textDecoration='none'"
                            ><b><?php echo $item['artist'] . ' '?></b><span style="color: grey"><?php echo $item['track']?></span></a></li>
                    <?php
                    //var_dump($item);
                }
                ?></ol> <?php
            }
        }
    }*/
}




